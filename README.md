# TODO

## Etapa 1
- Fazer a conexão com o banco -> OK
		
- implementar as telas -> OK
    - venda -> OK
    - relatórios -> OK
    - cadastro -> OK 
- CRUD -> OK
    - cadastro -> OK
    - venda -> OK
    - exclusão -> OK
    - alteração -> OK ( - imagem)
    
## Etapa 2 
- agrupar vendas iguais ( mesma data e produto ) -> OK
- habilitar alteração de imagem de um produto -> OK
- adaptar para modelo MVC -> OK
    - View -> OK
    - Model -> OK
    - Controller -> OK
- Refatorar Código -> OK
    - Reduzir funções  -> OK 
    - Rechecar necessidade de comentários -> OK
    - Diminuir visibilidades -> OK

		
