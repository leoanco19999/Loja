package br.lac.loja.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import br.lac.loja.model.Produto;
@SuppressWarnings("serial")
public class JTableModel extends AbstractTableModel{
    private static final String[] COLUMN_NAMES = new String[] {"Id", "Produto", "Preco", "Quantidade","Editar Imagem"};
    private static final Class<?>[] COLUMN_TYPES = new Class<?>[] {Integer.class, String.class, Float.class, Integer.class, JButton.class};
    private List<Produto> produtos = new ArrayList<>();
    
    public JTableModel(List<Produto> dados) {
    	for(Produto d : dados) {
    		this.produtos.add(d);
    	}
    }
    
    @Override public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override public int getRowCount() {
        return produtos.size();
    }

    @Override public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_TYPES[columnIndex];
    }
    

    
    @Override
    public boolean isCellEditable(int row, int column){ //Modifica a tabela impedindo de editar o ID
        return column != 0 && column != 4;
    }
    
    @Override public Object getValueAt(final int rowIndex, final int columnIndex) {
    	Produto produtoObj = produtos.get(rowIndex);
    	
        switch (columnIndex) {
            case 0: return produtoObj.getId();
            case 1: return produtoObj.getNome();
            case 2: return produtoObj.getPreco();
            case 3: return produtoObj.getQntd();
            case 4: final JButton button = new JButton(COLUMN_NAMES[columnIndex]);
                    button.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent arg0) {
                            JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(button), 
                                    "Button clicked for row "+rowIndex);
                        }
                    });
                    return button;
            default: return "Error";
        }
    }
    public void addProduto(Produto produto) {
    	insertProduto(getRowCount(), produto);
    }
    public void insertProduto(int row, Produto produto) {
    	produtos.add(row,produto);
    	fireTableRowsInserted(row, row);
    }
    public void removeProdutoDaTabela(int row) {
    	produtos.remove(row);
    	fireTableRowsDeleted(row, row);
    }
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    	switch(columnIndex) {
    		case 1:
    			if (aValue instanceof String) {
    				produtos.get(rowIndex).setNome(String.valueOf(aValue));
    			}
    			break;
    		case 2:
    			produtos.get(rowIndex).setPreco(Float.valueOf(String.valueOf(aValue)));
    			break;
    		case 3:
    			produtos.get(rowIndex).setQntd(Integer.valueOf(String.valueOf(aValue)));
    			break;
    	}
    	fireTableCellUpdated(rowIndex, rowIndex);
    }
}
