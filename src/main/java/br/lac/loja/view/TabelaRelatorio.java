package br.lac.loja.view;

import java.awt.Point;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.lac.loja.control.RelatorioController;
import br.lac.loja.model.Relatorio;

@SuppressWarnings("serial")
class TabelaRelatorio extends JPanel {

    private JTable tabela;
    private DefaultTableModel model = new DefaultTableModel();
    private static final Logger LOGGER = Logger.getLogger(TabelaRelatorio.class.getName());
    private List<Relatorio> relatorios;

    TabelaRelatorio(List<Relatorio> relatorios) {
        tabela = new JTable(model);
        this.relatorios = relatorios;

        preparaPainel();

        add(preparaScroll());
        criaColunas();

        tabela.setEnabled(false);
        preencheTabela(relatorios);
    }

    private void preencheTabela(List<Relatorio> relatorios) {
        while (model.getRowCount() > 0) {
            model.removeRow(0);
        }

        for (Relatorio r : relatorios) {
            LOGGER.info("Adicionando produto à tabela");
            Object[] dados = new Object[4];
            dados[0] = r.getDataVenda();
            dados[1] = r.getProduto();
            dados[2] = r.getValor();
            dados[3] = r.getQntd();
            model.addRow(dados);
        }
    }

    private void criaColunas() {
        model.addColumn("Data");
        model.addColumn("Produto");
        model.addColumn("Valor");
        model.addColumn("Quantidade");
    }

    private void preparaPainel() {
        setSize(350, 300);
        setLocation(new Point(250, 200));
    }

    private JScrollPane preparaScroll() {
        JScrollPane scroll = new JScrollPane(tabela);
        scroll.setBounds(20, 60, 320, 200);
        return scroll;
    }
}
