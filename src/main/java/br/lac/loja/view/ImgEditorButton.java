package br.lac.loja.view;

import java.awt.event.MouseAdapter;
import static br.lac.loja.common.Validator.btnEditaImgClicado;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTable;

import br.lac.loja.common.FileManager;
import br.lac.loja.control.ProdutoController;
import br.lac.loja.model.Produto;
import br.lac.loja.model.ProdutoDao;

public class ImgEditorButton extends MouseAdapter {
	private final JTable tabela;
	private static final Logger LOGGER = Logger.getLogger(ImgEditorButton.class.getName());

	public ImgEditorButton(JTable tabela) {
		this.tabela = tabela;
	}

	public void mouseClicked(MouseEvent e) {
		int column = tabela.getColumnModel().getColumnIndexAtX(e.getX());
		int row = e.getY() / tabela.getRowHeight();

		if (btnEditaImgClicado(column, row, tabela)) {
			Object value = tabela.getValueAt(row, column);
			if (value instanceof JButton) {
				int id = Integer.valueOf(String.valueOf(tabela.getValueAt(row, Coluna.ID.getColuna())));
				FileManager fm = new FileManager();
				fm.selectFileAndCopyFile();
				String novaImg = fm.getPath() + "/" + fm.getFileName();
				
				Produto produto = new Produto();
				produto.setImg(novaImg);
				produto.setId(id);
				new ProdutoController().alteraProduto(produto, novaImg); 
			}
		}
	}

}
