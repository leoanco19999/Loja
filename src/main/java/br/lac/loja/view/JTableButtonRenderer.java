package br.lac.loja.view;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class JTableButtonRenderer implements TableCellRenderer {        
    @Override 
    public Component getTableCellRendererComponent(JTable table, Object value, 
    		boolean isSelected, boolean hasFocus, int row, int column) {
        JButton button = (JButton)value;
        return button;  
    }
}
