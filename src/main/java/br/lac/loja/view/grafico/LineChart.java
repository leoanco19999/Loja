package br.lac.loja.view.grafico;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

import br.lac.loja.control.RelatorioController;
import br.lac.loja.model.Relatorio;

@SuppressWarnings("serial")
public class LineChart extends JPanel{

	public LineChart(String frameTitle, List<Relatorio> relatorios) {
		
		JFreeChart grafico = ChartFactory.createBarChart(frameTitle, "Data de Venda", 
				"Quantidade", getDataset(relatorios));
		ChartPanel chart = new ChartPanel(grafico);
		setLayout(new BorderLayout());
		setSize(new Dimension(300,450));
		add(chart, BorderLayout.CENTER);
	}
	
	private DefaultCategoryDataset getDataset(List<Relatorio> relatorios) {
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		for(Relatorio r : relatorios) {
			dataset.addValue(r.getQntd(), r.getProduto(), String.valueOf(r.getDataVenda()));
		}
		return dataset;
	}
}
