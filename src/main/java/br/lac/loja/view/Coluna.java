package br.lac.loja.view;

public enum Coluna {
   ID(0), PRODUTO(1), VALOR(2), QNTD(3);

    private final int col;

    Coluna(int col){ this.col = col;}

    public int getColuna(){ return col;}
}
