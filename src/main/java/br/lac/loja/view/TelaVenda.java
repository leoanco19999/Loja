package br.lac.loja.view;

import static br.lac.loja.common.Validator.isFieldEmpty;
import static br.lac.loja.common.Validator.isQntdValid;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

import br.lac.loja.control.ProdutoController;
import br.lac.loja.control.RelatorioController;
import br.lac.loja.model.Produto;
import br.lac.loja.model.Relatorio;

@SuppressWarnings("serial")
class TelaVenda extends JFrame {
	private JLabel labProduto;
	private JLabel labPreco;
	private JTable tabela;
	private JLabel labPic;
	private JLabel labCabecalho;
	private JLabel labAviso;
	private JTextField txtQntd;
	private DefaultTableModel model = new DefaultTableModel() {
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	private static final Logger LOGGER = Logger.getLogger(TelaVenda.class.getName());
	private List<String> imgs = new ArrayList<>();
	ProdutoController prodController = new ProdutoController();

	TelaVenda() {
		JLayeredPane pPrincipal = new JLayeredPane();

		tabela = new JTable(model);

		labProduto = new JLabel("Produto");
		labPreco = new JLabel("Preço");
		labAviso = new JLabel("Quantidade inválida");
		labCabecalho = new JLabel("Selecione um produto na tabela para o vender");

		txtQntd = new JTextField("Quantidade");
		txtQntd.setPreferredSize(new Dimension(100, 30));
		txtQntd.setMinimumSize(new Dimension(50, 30));
		txtQntd.setHorizontalAlignment(JTextField.CENTER);

		JPanel pVenda = new JPanel();
		JPanel pImg = new JPanel();
		JPanel pTabela = new JPanel();
		JPanel pCabecalho = new JPanel();
		JPanel pTopo = new JPanel();
		JPanel pButtons = new JPanel();

		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(e -> dispose());

		JButton btnVender = new JButton("Vender");
		btnVender.setEnabled(false);
		btnVender.addActionListener(e -> {
			Relatorio venda = new Relatorio();
			Produto produto = new Produto();
			RelatorioController relaController = new RelatorioController();

			String nomeProduto = tabela.getValueAt(tabela.getSelectedRow(), Coluna.PRODUTO.getColuna()).toString();
			int id = Integer.valueOf(tabela.getValueAt(tabela.getSelectedRow(), Coluna.ID.getColuna()).toString());
			Integer qntdAtual = Integer
					.valueOf(tabela.getValueAt(tabela.getSelectedRow(), Coluna.QNTD.getColuna()).toString());
			Float valVenda = Float
					.valueOf(tabela.getValueAt(tabela.getSelectedRow(), Coluna.VALOR.getColuna()).toString());
			Date dataVenda = new Date(System.currentTimeMillis());

			produto.setNome(nomeProduto);
			produto.setPreco(valVenda);
			produto.setId(id);

			venda.setProduto(nomeProduto);
			venda.setDataVenda(dataVenda);


			LOGGER.info("Vendendo " + txtQntd.getText() + " " + nomeProduto);

			int qntdEstoque = qntdAtual - Integer.valueOf(txtQntd.getText());
			int qntdVendida = Integer.valueOf(txtQntd.getText());

			valVenda *= qntdVendida;
			venda.setQntd(qntdVendida);
			System.out.println(qntdVendida);
			venda.setValor(valVenda);

			relaController.geraRelatorio(venda);
			prodController.alteraProduto(produto, qntdEstoque);

			tabela.setValueAt(Integer.valueOf(String.valueOf(qntdEstoque)), tabela.getSelectedRow(),
					Coluna.QNTD.getColuna());

		});
		tabela.setSize(350, 300);
		tabela.setLocation(new Point(250, 200));

		DocumentListener documentListener = new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}

			public void insertUpdate(DocumentEvent e) {
				validaInputDeQntd(btnVender);
			}

			public void removeUpdate(DocumentEvent e) {
				validaInputDeQntd(btnVender);
			}
		};
		txtQntd.getDocument().addDocumentListener(documentListener);

		JScrollPane scroll = new JScrollPane(tabela);
		scroll.setBounds(20, 60, 320, 200);

		try {
			BufferedImage myPicture = ImageIO.read(new File("src/main/resources/img/default.png"));
			labPic = new JLabel(new ImageIcon(myPicture));
			pImg.add(labPic);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Erro: {}", e);
		}

		setTitle("Venda");

		pPrincipal.setLayout(new BoxLayout(pPrincipal, BoxLayout.Y_AXIS));

		pTopo.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5, 5, 5, 5);
		c.gridy = 10;
		pTopo.add(labProduto, c);
		c.gridy = 20;
		pTopo.add(labPreco, c);
		c.gridy = 30;
		pTopo.add(txtQntd, c);

		pVenda.setLayout(new GridLayout(1, 2));
		pVenda.add(pImg);
		pVenda.add(pTopo);

		pTabela.setLayout(new BorderLayout());
		pTabela.add(scroll);

		pCabecalho.add(btnVoltar);
		pCabecalho.add(labCabecalho);
		pCabecalho.setLayout(new FlowLayout(FlowLayout.LEFT));

		pButtons.add(btnVender);
		pButtons.add(labAviso);
		labAviso.setVisible(false);

		model.addColumn("id");
		model.addColumn("Produto");
		model.addColumn("Preço");
		model.addColumn("Quantidade");

		initTable();

		pPrincipal.add(pCabecalho, 4, 0);
		pPrincipal.add(pVenda, 3, 0);
		pPrincipal.add(pButtons, 2, 0);
		pPrincipal.add(pTabela, 1, 0);

		event();

		add(pPrincipal);
	}

	private void setLabels(String nome, String preco, String qntd) {
		labProduto.setText(nome);
		labPreco.setText(preco);
		txtQntd.setText(qntd);
	}

	private void event() {
		tabela.getSelectionModel().addListSelectionListener(this::tabelaHandler);
	}

	private void tabelaHandler(ListSelectionEvent e) {
		int linhaSelecionada = tabela.getSelectedRow();
		Object data[] = new Object[4];
		if (linhaSelecionada != -1) {
			for (int i = 0; i < 4; i++) {
				data[i] = tabela.getModel().getValueAt(linhaSelecionada, i);
			}
			try {
				BufferedImage img = ImageIO.read(new File(imgs.get(linhaSelecionada)));
				Image resizedImg = img.getScaledInstance(labPic.getWidth(), labPic.getHeight(), Image.SCALE_SMOOTH);
				labPic.setIcon(new ImageIcon(resizedImg));
			} catch (IOException ex) {
				LOGGER.log(Level.SEVERE, "Erro: {}", ex);
			}
		}
		setLabels(String.valueOf(data[1]), "R$ " + String.valueOf(data[2]), String.valueOf(data[3]));
	}

	private void initTable() {
		while (model.getRowCount() > 0) {
			model.removeRow(0);
		}

		List<Produto> produtos = prodController.geraLista();
		for (Produto p : produtos) {
			LOGGER.info("Adicionando produto à tabela");
			Object[] dados = new Object[4];
			dados[0] = p.getId();
			dados[1] = p.getNome();
			dados[2] = formataDecimal(p.getPreco());
			dados[3] = p.getQntd();
			imgs.add(p.getImg());
			model.addRow(dados);
		}
	}

	private String formataDecimal(Float decimal) {
		DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
		dfs.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat(".00", dfs);

		return df.format(decimal);
	}

	private void validaInputDeQntd(JButton btn) {
		if (isQntdValid(txtQntd.getText())) {
			if (tabela.getSelectedRow() != -1) {
				labCabecalho.setVisible(false);
				if (!isFieldEmpty(txtQntd)) {
					if (isQntdDisponivel()) {
						btn.setEnabled(true);
						labAviso.setVisible(false);
					} else
						btn.setEnabled(false);
				}
			} else
				btn.setEnabled(false);
		} else
			btn.setEnabled(false);

		seBotaoDesabilitadoExibeAviso(btn);
	}

	private void seBotaoDesabilitadoExibeAviso(JButton btn) {
		if (!btn.isEnabled() && !txtQntd.getText().equals("Quantidade"))
			labAviso.setVisible(true);
	}

	private boolean isQntdDisponivel() {
		int qntdTabela = Integer
				.valueOf(tabela.getValueAt(tabela.getSelectedRow(), Coluna.QNTD.getColuna()).toString());
		int qntdInput = Integer.valueOf(txtQntd.getText());

		return !(qntdInput > qntdTabela || qntdInput <= 0);
	}
}
