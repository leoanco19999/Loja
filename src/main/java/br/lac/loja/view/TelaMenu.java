package br.lac.loja.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class TelaMenu extends JFrame{
    private JFrame janMenu;
    private static final Logger LOGGER = Logger.getLogger(TelaMenu.class.getName());
    
    public TelaMenu(){
        preparaJanela();
        preparaPainelPrincipal();
    }
    private void preparaJanela() {
        janMenu = new JFrame("Menu");
        janMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public void mostraJanela(){
        janMenu.pack();
        janMenu.setSize(220,350);
        janMenu.setResizable(false);
        janMenu.setLocationRelativeTo(null);
        janMenu.setVisible(true);
    }
    private void preparaPainelPrincipal(){
        JPanel painelPrincipal = new JPanel();
        painelPrincipal.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridheight = 10;
        c.gridwidth = 10;
        c.gridx = 10;
        c.gridy = 10;
        c.weightx = 10;
        c.weighty = 10;
        painelPrincipal.add(preparaBotaoCadastro(), c);
        c.gridy += 10;
        painelPrincipal.add(preparaBotaoVenda(), c);
        c.gridy += 10;
        painelPrincipal.add(preparaBotaoRelatorios(), c);
        c.gridy += 10;
        painelPrincipal.add(preparaBotaoSair(), c);
        painelPrincipal.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        janMenu.add(painelPrincipal);
    }
    private JButton preparaBotaoVenda(){
        JButton botaoVenda = new JButton("Vender");
        botaoVenda.addActionListener(e -> {
            LOGGER.info("Criando tela de venda...");
            TelaVenda telaVenda = new TelaVenda();
            setTela(telaVenda);
        });
        setMenuButtonsConfig(botaoVenda);
        return botaoVenda;
    }
    private JButton preparaBotaoRelatorios(){
        JButton botaoRelatorios = new JButton("Relatórios");
        botaoRelatorios.addActionListener(e -> {
            LOGGER.info("Criando tela de relatórios...");
            TelaRelatorio janRelatorios = new TelaRelatorio();
            setTela(janRelatorios);
        });
        setMenuButtonsConfig(botaoRelatorios);
        return botaoRelatorios;
    }
    private JButton preparaBotaoCadastro(){
        JButton botaoCadastro = new JButton("Cadastrar");
        botaoCadastro.addActionListener(e -> {
            LOGGER.info("Criando tela de cadastro...");
            TelaCadastro telaCadastro = new TelaCadastro();
            setTela(telaCadastro);
        });
        setMenuButtonsConfig(botaoCadastro);
        return botaoCadastro;
    }
    private JButton preparaBotaoSair(){
        JButton botaoSair = new JButton("Sair");
        botaoSair.addActionListener(e -> {
            LOGGER.info("Finalizando...");
            System.exit(0);
        });
        setMenuButtonsConfig(botaoSair);
        return botaoSair;
    }
    private void setMenuButtonsConfig(JButton btn){
        btn.setPreferredSize(new Dimension(120, 40));
        btn.setAlignmentX(Component.CENTER_ALIGNMENT);
    }
    private void setTela(JFrame janela){
        janMenu.setEnabled(false);
        janela.pack();
        janela.setSize(700, 500);
        janela.setLocationRelativeTo(null);
        janela.setVisible(true);
        janela.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                janela.dispose();
                janMenu.setEnabled(true);
            }
            @Override
            public void windowClosed(WindowEvent e){
                janMenu.setEnabled(true);
                janMenu.requestFocus();
            }
        });
    }
}
