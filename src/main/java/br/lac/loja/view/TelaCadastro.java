package br.lac.loja.view;

import static br.lac.loja.common.Validator.isComboBoxEmpty;
import static br.lac.loja.common.Validator.isDuplicate;
import static br.lac.loja.common.Validator.isOnTable;
import static br.lac.loja.common.Validator.isPrecoValid;
import static br.lac.loja.common.Validator.isProdValid;
import static br.lac.loja.common.Validator.isQntdValid;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableCellRenderer;

import br.lac.loja.common.FileManager;
import br.lac.loja.control.ProdutoController;
import br.lac.loja.model.Produto;
import br.lac.loja.model.ProdutoDao;

@SuppressWarnings("serial")
public class TelaCadastro extends JFrame {
	private static final Logger LOGGER = Logger.getLogger(TelaCadastro.class.getName());
	private BufferedImage myPicture;
	private String imgPath;
	private static JComboBox<String> boxProduto;
	private JTable tabela;
	private String nomeTabela = "";
	private float precoTabela = 0;
	private int qntdTabela = 0;
	private JTableModel model = new JTableModel(new ProdutoDao().getLista());
	private ProdutoController controller = new ProdutoController();

	TelaCadastro() {

		tabela = new JTable(model);
		initTable();

		JLabel labPic;
		JLabel labProduto = new JLabel("Produto");
		JLabel labPreco = new JLabel("Preço");
		JLabel labQntd = new JLabel("Quantidade");

		boxProduto = new JComboBox<>(getNomes());
		JTextField txtPreco = new JTextField();
		JTextField txtQntd = new JTextField();

		JLayeredPane pPrincipal = new JLayeredPane();
		JPanel pCadastro = new JPanel();
		JPanel pTopo = new JPanel();
		JPanel pBtns = new JPanel();
		JPanel pCabecalho = new JPanel();
		JPanel pTabela = new JPanel();
		JPanel pImg = new JPanel();

		JButton btnImg = new JButton("Add Imagem");
		JButton btnVoltar = new JButton("Voltar");
		JButton btnAdd = new JButton("...");
		JButton btnCadastro = new JButton("Cadastrar");
		JButton btnRemove = new JButton("Remover");

		btnVoltar.addActionListener(e -> dispose());
		btnRemove.addActionListener(e -> remove(this));

		btnAdd.addActionListener(e -> {
			JFrame telaAdd = new JFrame("Adicionar Produto");
			JPanel pAdd = new JPanel();
			JButton btnConfirma = new JButton("Adicionar à lista");
			btnConfirma.setEnabled(false);
			JLabel labAdd = new JLabel("Produto");
			JTextField txtAdd = new JTextField();
			defineTamanho(labAdd, 70);
			txtAdd.setPreferredSize(new Dimension(150, 25));
			txtAdd.setMinimumSize(new Dimension(150, 25));

			btnConfirma.addActionListener(ev -> boxProduto.addItem(txtAdd.getText()));
			DocumentListener docAdd = new DocumentListener() {
				public void changedUpdate(DocumentEvent e) {
				}

				public void insertUpdate(DocumentEvent e) {
					validaInputAoComboBox(txtAdd, boxProduto, btnConfirma);
				}

				public void removeUpdate(DocumentEvent e) {
					validaInputAoComboBox(txtAdd, boxProduto, btnConfirma);
				}
			};
			txtAdd.getDocument().addDocumentListener(docAdd);
			pAdd.add(labAdd);
			pAdd.add(txtAdd);
			pAdd.add(btnConfirma);

			telaAdd.add(pAdd);
			telaAdd.pack();
			telaAdd.setSize(300, 100);
			telaAdd.setLocationRelativeTo(null);
			telaAdd.setVisible(true);
		});
		boxProduto.addActionListener(e -> validaCadastro(txtPreco, txtQntd, btnCadastro));

		// Adiciona imagem à label e altera com o botão AddImagem
		try {
			imgPath = "src/main/resources/img/default.png";
			myPicture = ImageIO.read(new File(imgPath));
			labPic = new JLabel(new ImageIcon(myPicture));
			labPic.setPreferredSize(new Dimension(128, 128));
			labPic.setMaximumSize(new Dimension(128, 128));
			pImg.add(labPic);
			btnImg.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					try {
						FileManager fm = new FileManager();
						fm.selectFileAndCopyFile();
						imgPath = fm.getPath() + "/" + fm.getFileName();
						myPicture = ImageIO.read(new File(imgPath));
						Image resizedImg = myPicture.getScaledInstance(labPic.getWidth(), 
																	labPic.getHeight(),
																	Image.SCALE_SMOOTH);
						labPic.setIcon(new ImageIcon(resizedImg));
					} catch (IOException e1) {
						LOGGER.log(Level.SEVERE, "Erro: {}", e1);
					}

				}
			});
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Erro: {}", e);
		}

		btnCadastro.addActionListener(e -> {
			if (!produtoRepetido()) {
				Produto prod = new Produto();
				prod.setNome(String.valueOf(boxProduto.getSelectedItem()));
				prod.setPreco(Float.valueOf(txtPreco.getText()));
				prod.setQntd(Integer.valueOf(txtQntd.getText()));
				prod.setImg(imgPath);
				int lastId = controller.cadastraProduto(prod);
				prod.setId(lastId);
				adicionaNaTabela(prod);
			} else {
				JOptionPane.showMessageDialog(this,
						"Este produto já foi cadastrado, "
								+ "para alterar um produto clique duas vezes na cédula da tabela do produto",
						"Erro", JOptionPane.ERROR_MESSAGE);
			}
		});
		// Verifica em tempo real se o produto pode ser cadastrado
		DocumentListener documentListener = new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}

			public void insertUpdate(DocumentEvent e) {
				validaCadastro(txtPreco, txtQntd, btnCadastro);
			}

			public void removeUpdate(DocumentEvent e) {
				validaCadastro(txtPreco, txtQntd, btnCadastro);
			}
		};

		// Adiciona verificadores p/ verificar input em tempo real
		txtPreco.getDocument().addDocumentListener(documentListener);
		txtQntd.getDocument().addDocumentListener(documentListener);
		btnCadastro.setEnabled(false);

		defineTamanho(boxProduto, 160);
		defineTamanho(txtPreco, 70);
		defineTamanho(txtQntd, 70);

		pCabecalho.add(btnVoltar);
		pCabecalho.setLayout(new FlowLayout(FlowLayout.LEFT));

		pCadastro.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5, 5, 5, 5);
		c.gridy = 10;
		pCadastro.add(labProduto, c);
		pCadastro.add(boxProduto, c);
		pCadastro.add(btnAdd, c);
		c.gridy = 20;
		pCadastro.add(labPreco, c);
		pCadastro.add(txtPreco, c);
		c.gridy = 30;
		pCadastro.add(labQntd, c);
		pCadastro.add(txtQntd, c);

		pTopo.setLayout(new GridLayout(1, 2));
		pTopo.add(pImg);
		pTopo.add(pCadastro);

		pBtns.add(btnImg);
		pBtns.add(btnCadastro);
		pBtns.add(btnRemove);

		JScrollPane scroll = new JScrollPane(tabela);
		scroll.setBounds(20, 60, 320, 400);
		pTabela.add(scroll);

		pPrincipal.setBounds(0, 0, 600, 600);
		pPrincipal.setLayout(new BoxLayout(pPrincipal, BoxLayout.Y_AXIS));
		pPrincipal.add(pCabecalho, 3, 0);
		pPrincipal.add(pTopo, 2, 0);
		pPrincipal.add(pBtns, 1, 0);
		pPrincipal.add(pTabela, 0, 0);

		setTitle("Cadastro");
		setLayout(new BorderLayout());
		add(pPrincipal, BorderLayout.CENTER);
		add(pPrincipal);
	}

	private void defineTamanho(Component field, int width) {
		field.setPreferredSize(new Dimension(width, 25));
		field.setMinimumSize(new Dimension(width, 25));
	}

	private void validaCadastro(JTextField preco, JTextField qntd, JButton btn) {
		if (isPrecoValid(preco.getText()) && isQntdValid(qntd.getText()) && !isComboBoxEmpty(boxProduto))
			btn.setEnabled(true);
		else
			btn.setEnabled(false);
	}

	private void validaInputAoComboBox(JTextField prod, JComboBox<String> box, JButton btn) {
		if (isProdValid(prod.getText()) && !isDuplicate(prod.getText(), box))
			btn.setEnabled(true);
		else
			btn.setEnabled(false);
	}

	static void attBox(String novoProd, String velhoProd) {
		for (int i = 0; i < boxProduto.getItemCount(); i++) {
			if (boxProduto.getItemAt(i).equals(velhoProd)) {
				boxProduto.removeItemAt(i);
				boxProduto.addItem(novoProd);
			}
		}
	}

	private String[] getNomes() {
		String[] nomes = new String[tabela.getRowCount()];
		for (int i = 0; i < tabela.getRowCount(); i++) {
			nomes[i] = String.valueOf(tabela.getValueAt(i, 1));
		}
		return nomes;
	}

	private void adicionaNaTabela(Produto prod) {
		model.addProduto(prod);
	}

	private void remove(JFrame frame) {
		int linhaSelecionada = tabela.getSelectedRow();
		if (linhaSelecionada != -1) {
			Object id = model.getValueAt(linhaSelecionada, Coluna.ID.getColuna());
			String nome = String.valueOf(model.getValueAt(linhaSelecionada, Coluna.PRODUTO.getColuna()));
			Produto produto = new Produto();
			produto.setNome(nome);
			produto.setId(Integer.valueOf(id.toString()));

			controller.removeProduto(produto);
			model.removeProdutoDaTabela(linhaSelecionada);
		} else {
			JOptionPane.showMessageDialog(frame, "Nenhuma linha da tabela selecionada!", "Erro",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void initTable() {
		addListeners();

		tabela.setFillsViewportHeight(true);
		TableCellRenderer buttonRenderer = new JTableButtonRenderer();
		tabela.getColumn("Editar Imagem").setCellRenderer(buttonRenderer);
		tabela.addMouseListener(new ImgEditorButton(tabela));
	}

	private void addListeners() {

		tabela.addPropertyChangeListener(evt -> {
			if ("tableCellEditor".equals(evt.getPropertyName())) {
				int row = tabela.getSelectedRow();
				int col = tabela.getSelectedColumn();

				int id = Integer.valueOf(tabela.getValueAt(row, Coluna.ID.getColuna()).toString());
				Produto produto = new Produto();
				produto.setId(id);

				if (tabela.isEditing()) {
					LOGGER.info("Editando linha: " + row + " coluna: " + col);
					nomeTabela = String.valueOf(tabela.getValueAt(row, Coluna.PRODUTO.getColuna()));
					precoTabela = Float.valueOf(tabela.getValueAt(row, Coluna.VALOR.getColuna()).toString());
					qntdTabela = Integer.valueOf(tabela.getValueAt(row, Coluna.QNTD.getColuna()).toString());

					produto.setNome(nomeTabela);
					produto.setPreco(precoTabela);
					produto.setQntd(qntdTabela);

				} else {
					LOGGER.info("Validando edição...");

					String nomeInput;
					float precoInput;
					int qntdInput;

					if (col == 1 && isProdValid(String.valueOf(tabela.getValueAt(row, col)))) {
						nomeInput = String.valueOf(tabela.getValueAt(row, col));
						produto.setNome(nomeInput);
						controller.alteraProduto(produto, nomeInput);
						TelaCadastro.attBox(nomeInput, nomeTabela);
						LOGGER.info("Novo nome: " + nomeInput);
					} else if (col == 2 && isPrecoValid((tabela.getValueAt(row, col).toString()))) {
						precoInput = Float.valueOf(tabela.getValueAt(row, col).toString());
						produto.setPreco(precoInput);
						controller.alteraProduto(produto, precoInput);
						LOGGER.info("Novo preco: " + precoInput);
					} else if (col == 3 && isQntdValid((tabela.getValueAt(row, col).toString()))) {
						qntdInput = Integer.valueOf(tabela.getValueAt(row, col).toString());
						produto.setQntd(qntdInput);
						controller.alteraProduto(produto, qntdInput);
						LOGGER.info("Nova qntd: " + qntdInput);
					} else {
						LOGGER.info("Erro ao validar input");
						tabela.setValueAt(nomeTabela, tabela.getSelectedRow(), 1);
						tabela.setValueAt(precoTabela, tabela.getSelectedRow(), 2);
						tabela.setValueAt(qntdTabela, tabela.getSelectedRow(), 3);
					}
				}
			}
		});
	}

	private boolean produtoRepetido() {
		return isOnTable(String.valueOf(boxProduto.getSelectedItem()), tabela);
	}
}
