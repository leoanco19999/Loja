package br.lac.loja.view;

import java.awt.FlowLayout;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import br.lac.loja.control.RelatorioController;
import br.lac.loja.model.Relatorio;
import br.lac.loja.view.grafico.LineChart;

@SuppressWarnings("serial")
class TelaRelatorio extends JFrame{
	private final RelatorioController controller = new RelatorioController();
	private final List<Relatorio> relatorios = controller.geraLista();

    TelaRelatorio(){ add(preparaPPrincipal());}

    private JButton preparaBtnVoltar(){
        JButton btnVoltar = new JButton("Voltar");
        btnVoltar.addActionListener(e -> dispose());

        return btnVoltar;
    }
    private JPanel preparaPCabecalho(){
        JPanel pCabecalho = new JPanel();
        pCabecalho.add(preparaBtnVoltar());
        pCabecalho.setLayout(new FlowLayout(FlowLayout.LEFT));

        return pCabecalho;
    }
    private JLayeredPane preparaPPrincipal(){
    	TabelaRelatorio tabela = new TabelaRelatorio(relatorios);
    	LineChart pGrafico = new LineChart("Vendas por data", relatorios);
        JLayeredPane pPrincipal = new JLayeredPane();
        pPrincipal.setLayout(new BoxLayout(pPrincipal, BoxLayout.Y_AXIS));
        pPrincipal.add(preparaPCabecalho(), 1, 0);
        pPrincipal.add(tabela, 0, 0);
        pPrincipal.add(pGrafico, 0, 0);

        return pPrincipal;
    }
}
