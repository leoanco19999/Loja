package br.lac.loja.common;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.swing.JFileChooser;

import org.apache.commons.io.FileUtils;

public class FileManager {
	private final String PATH = "src/main/resources/img";
	private File file;
	private static final Logger LOGGER = Logger.getLogger(FileManager.class.getName());
	
	public void copyFileToPath() {
		File fileCopy = new File(PATH);
		try {
			FileUtils.copyFileToDirectory(file, fileCopy);
		} catch (IOException e) {
			LOGGER.info("Erro ao copiar imagem");
		}
	}
	
	public void selectFileAndCopyFile() {
		JFileChooser fc = new JFileChooser();
		int result = fc.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION) {
			this.file = fc.getSelectedFile();
			copyFileToPath();
		}
	}
	public String getPath() {
		return this.PATH;
	}
	public String getFileName() {
		return this.file.getName();
	}
}
