package br.lac.loja.common;

import javax.swing.*;

import br.lac.loja.view.Coluna;

public final class Validator {
    public static boolean isProdValid(String prod) {
        return prod.matches("^(([A-Za-zÀ-ÿ])\\s?[-]?)+$");
    }

    public static boolean isPrecoValid(String preco) {
        if (preco.matches("^(\\d)+[.]?(\\d)*$")) {
            return Float.valueOf(preco) > 0;
        } else
            return false;
    }

    public static boolean isQntdValid(String qntd) {
        if (qntd.matches("^(\\d)+$")) {
            return Integer.valueOf(qntd) > 0;
        } else
            return false;
    }

    public static boolean isDuplicate(String txt, JComboBox<String> box) {
        for (int i = 0; i < box.getItemCount(); i++) {
            if (txt.equals(box.getItemAt(i)))
                return true;
        }
        return false;
    }

    public static boolean isOnTable(String str, JTable table) {
        for (int i = 0; i < table.getRowCount(); i++) {
            if (str.equals(table.getValueAt(i, Coluna.PRODUTO.getColuna())))
                return true;
        }
        return false;
    }

    public static boolean isFieldEmpty(JTextField field) {
        return field.getText().equals("");
    }
    public static boolean isComboBoxEmpty(JComboBox<String> box){
        return box.getItemCount() < 1;
    }
	public static boolean btnEditaImgClicado(int column, int row, JTable tabela) {
		return row < tabela.getRowCount() && row >= 0 && column < tabela.getColumnCount() && column >= 0;
	}
}
