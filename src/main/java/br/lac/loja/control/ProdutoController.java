package br.lac.loja.control;

import java.util.List;
import java.util.logging.Logger;

import br.lac.loja.model.Produto;
import br.lac.loja.model.ProdutoDao;

public class ProdutoController {
	private ProdutoDao dao;
	
	private static final Logger LOGGER = Logger.getLogger(ProdutoController.class.getName());
	
	public ProdutoController() {
		this.dao = new ProdutoDao();
	}
	
	public int cadastraProduto(Produto produto) {
		LOGGER.info("Cadastrando " + produto.getNome() + "...");
		return dao.adiciona(produto);
	}
	public void removeProduto(Produto produto) {
		LOGGER.info("Removendo " + produto.getNome() + "...");
		dao.deleta(produto.getId());
	}
	public void alteraProduto(Produto produto, Object novoValor) {
		LOGGER.info("Novo valor: " + novoValor);
		dao.altera(produto, novoValor);
	}
	public List<Produto> geraLista(){
		LOGGER.info("Gerando lista de produtos");
		return dao.getLista();
	}
}
