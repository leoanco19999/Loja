package br.lac.loja.control;

import java.util.List;
import java.util.logging.Logger;

import br.lac.loja.model.Relatorio;
import br.lac.loja.model.RelatorioDao;

public class RelatorioController {
	private RelatorioDao dao;
	
	private static final Logger LOGGER = Logger.getLogger(ProdutoController.class.getName());
	
	public RelatorioController() {
		this.dao = new RelatorioDao();
	}
	
	public void geraRelatorio(Relatorio relatorio) {
		LOGGER.info("Geranto relatorio de " + relatorio.getProduto() + "...");
		dao.adiciona(relatorio);
	}
	public List<Relatorio> geraLista(){
		LOGGER.info("Gerando lista de relatorios");
		return dao.getListaRelatorio();
	}
	public List<Relatorio> geraListaParaGrafico(){
		LOGGER.info("Gerando lista para tabelas");
		return dao.agrupaProdutosDeMesmaData(dao.getListaRelatorio());
	}
}
