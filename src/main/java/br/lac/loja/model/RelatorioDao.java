package br.lac.loja.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RelatorioDao {
	private static final Logger LOGGER = Logger.getLogger(RelatorioDao.class.getName());
	
    public List<Relatorio> getListaRelatorio(){
        String sql = "SELECT * FROM relatorios";
        try(Connection con = new ConnectionFactory().getConnection();
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery()) {
            List<Relatorio> relatorios = new ArrayList<>();
            while(rs.next()){
                Relatorio relatorio = new Relatorio();
                relatorio.setProduto(rs.getString("rel_produto"));
                relatorio.setValor(rs.getFloat("rel_valortotal"));
                relatorio.setQntd(rs.getInt("rel_qntdvendida"));
                relatorio.setDataVenda(rs.getDate("rel_datavenda"));
                relatorios.add(relatorio);
            }
            return agrupaProdutosDeMesmaDataENome(relatorios);
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
    }
    
    public void adiciona(Relatorio relatrorio) {
        String sql = "INSERT INTO relatorios(REL_PRODUTO, REL_VALORTOTAL, REL_QNTDVENDIDA, REL_DATAVENDA) VALUES (?,?,?,?)";
        try(Connection con = new ConnectionFactory().getConnection();
            PreparedStatement pstm = con.prepareStatement(sql)) {
            pstm.setString(1, relatrorio.getProduto());
            pstm.setFloat(2, relatrorio.getValor());
            pstm.setInt(3, relatrorio.getQntd());
            pstm.setDate(4, relatrorio.getDataVenda());
            pstm.execute();
            LOGGER.info("Relatório feito com sucesso");

        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Erro: {}", e);
        }
    }
    
    private List<Relatorio> agrupaProdutosDeMesmaDataENome(List<Relatorio> relatorios) {
        for (int j = 0; j < relatorios.size(); j++) {
            List<Integer> duplicados = new ArrayList<>();
            Relatorio r = relatorios.get(j);
            for (int i = j + 1; i < relatorios.size(); i++) {
                Relatorio r2 = relatorios.get(i);
                if (mesmaDataEProduto(r, r2)) {

                    LOGGER.log(Level.INFO, "Juntando: {0}\n índices: j={1} "
                            + "i={2}", new Object[]{r.getProduto(), j, i});

                    somaValorEQntd(r, r2);
                    duplicados.add(i);
                }      
            }
            removeCopias(relatorios, duplicados);
        }
        return relatorios;
    }
    
    public List<Relatorio> agrupaProdutosDeMesmaData(List<Relatorio> relatorios) {
        for (int j = 0; j < relatorios.size(); j++) {
            List<Integer> duplicados = new ArrayList<>();
            Relatorio r = relatorios.get(j);
            for (int i = j + 1; i < relatorios.size(); i++) {
                Relatorio r2 = relatorios.get(i);
                if (r.getDataVenda().compareTo(r2.getDataVenda()) == 0) {

                    LOGGER.log(Level.INFO, "Juntando: {0}\n índices: j={1} "
                            + "i={2}", new Object[]{r.getProduto(), j, i});

                    somaValorEQntd(r, r2);
                    duplicados.add(i);
                }      
            }
            removeCopias(relatorios, duplicados);
        }
        return relatorios;
    }

	private void removeCopias(List<Relatorio> relatorios, List<Integer> duplicados) {
		if(!duplicados.isEmpty()){
		    LOGGER.info("Removendo índeces: " 
		                + duplicados.toString());
		    for(int index : duplicados){
		        relatorios.remove(index);
		        for(int i = 0; i < duplicados.size(); i++){
		            duplicados.set(i, duplicados.get(i) - 1);
		        }
		    }
		    duplicados.clear();
		}
		LOGGER.info(duplicados.toString());
	}
    
    private void somaValorEQntd(Relatorio r, Relatorio o) {
        r.setQntd(r.getQntd() + o.getQntd());
        r.setValor(r.getValor() + o.getValor());
    }

    private boolean mesmaDataEProduto(Relatorio r, Relatorio o) {
        return r.getDataVenda().compareTo(o.getDataVenda()) == 0
                && r.getProduto().equals(o.getProduto());
    }
}
