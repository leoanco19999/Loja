package br.lac.loja.model;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import java.util.logging.Logger;

public class ProdutoDao {
    private static final Logger LOGGER = Logger.getLogger(ProdutoDao.class.getName());


    public List<Produto> getLista(){
        String sql = "SELECT * FROM produtos";
        try(Connection con = new ConnectionFactory().getConnection();
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery()) {
            List<Produto> produtos = new ArrayList<>();
            while(rs.next()){
                Produto produto = new Produto();
                produto.setId(rs.getInt("prod_id"));
                produto.setNome(rs.getString("prod_nome"));
                produto.setPreco(rs.getFloat("prod_preco"));
                produto.setQntd(rs.getInt("prod_qntd"));
                produto.setImg(rs.getString("prod_img"));
                produtos.add(produto);
            }
            return produtos;
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
    }
    public int adiciona(Produto produto) {
        String sql = "INSERT INTO produtos(PROD_NOME, PROD_PRECO, PROD_QNTD, PROD_IMG) VALUES (?,?,?,?)";
        try(Connection con = new ConnectionFactory().getConnection();
            PreparedStatement pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            pstm.setString(1, produto.getNome());
            pstm.setFloat(2, produto.getPreco());
            pstm.setInt(3, produto.getQntd());
            pstm.setString(4, produto.getImg());
            pstm.execute();
            LOGGER.info("Produto inserido com sucesso");
            int lastId = 0;
            ResultSet rs = pstm.getGeneratedKeys();
            if(rs.next())
                lastId = rs.getInt(1);

            rs.close();
            return lastId;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Erro: {}", e);
        }
        return -1;
    }

    public void deleta(int id) {
        String sql = "DELETE FROM produtos WHERE prod_id=?";
        try(Connection con = new ConnectionFactory().getConnection();
            PreparedStatement pstm = con.prepareStatement(sql)) {
            pstm.setInt(1, id);
            pstm.execute();
            LOGGER.info("Produto removido com sucesso");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Erro: {}", e);
        }
    }
    public void altera(Produto produto, Object novoValor) {
    	int id = produto.getId();
    	
    	if(novoValor instanceof Integer) {
			Integer novaQntd = (Integer) novoValor;
			altera(novaQntd, id);
		} else if (novoValor instanceof Float) {
			Float novoPreco = (Float) novoValor;
			altera(novoPreco, id);
		} else if (novoValor instanceof String) {
			String novaString = (String) novoValor;
			if(novaString.contains("/"))
				alteraImg(novaString, id);
			else
				altera(novaString, id);
		} else {
			LOGGER.info("Novo valor de um tipo inválido");
		}
    }
    private void altera(Integer qntd, int id){
        String sql = "UPDATE produtos SET prod_qntd=? WHERE prod_id=?";
        try(Connection con = new ConnectionFactory().getConnection();
            PreparedStatement pstm = con.prepareStatement(sql)){
            pstm.setInt(1, qntd);
            pstm.setInt(2, id);
            pstm.execute();
            LOGGER.info("Alterando quantidade da tabela produtos \n Id alterado: " + id);
        }catch(SQLException e){
            LOGGER.log(Level.SEVERE, "Error: {}", e);
        }
    }
    private void altera(String nome, int id){
        String sql = "UPDATE produtos SET prod_nome=? WHERE prod_id=?";
        try(Connection con = new ConnectionFactory().getConnection();
            PreparedStatement pstm = con.prepareStatement(sql)){
            pstm.setString(1, nome);
            pstm.setInt(2, id);
            pstm.execute();
            LOGGER.info("Alterando nome de produto da tabela produtos \n Id alterado: " + id);
        }catch(SQLException e){
            LOGGER.log(Level.SEVERE, "Error: {}", e);
        }
    }
    public void alteraImg(String img, int id){
        String sql = "UPDATE produtos SET prod_img=? WHERE prod_id=?";
        try(Connection con = new ConnectionFactory().getConnection();
            PreparedStatement pstm = con.prepareStatement(sql)){
            pstm.setString(1, img);
            pstm.setInt(2, id);
            pstm.execute();
            LOGGER.info("Alterando imagem de produto da tabela produtos \n Id alterado: " + id);
        }catch(SQLException e){
            LOGGER.log(Level.SEVERE, "Error: {}", e);
        }
    }
    private void altera(float preco, int id){
        String sql = "UPDATE produtos SET prod_preco=? WHERE prod_id=?";
        try(Connection con = new ConnectionFactory().getConnection();
            PreparedStatement pstm = con.prepareStatement(sql)){
            pstm.setFloat(1, preco);
            pstm.setInt(2, id);
            pstm.execute();
            LOGGER.info("Alterando preco da tabela produtos \n Id alterado: " + id);
        }catch(SQLException e){
            LOGGER.log(Level.SEVERE, "Error: {}", e);
        }
    }
}
