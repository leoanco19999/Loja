package br.lac.loja.model;

public class Produto {
    private int id;
    private String nome;
    private float preco;
    private int qntd;
    private String img;

    public String getImg() {
        return img;
    }

    public void setImg(String img){ this.img = img;}

    public int getId() {
        return id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getPreco() { return preco; }

    public void setPreco(float preco) { this.preco = preco; }
    
    public int getQntd() {
        return qntd;
    }

    public void setQntd(int qntd) {
        this.qntd = qntd;
    }
}
