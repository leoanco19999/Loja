package br.lac.loja.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionFactory {
    private Connection con;
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    public Connection getConnection() {
        try {
            if (con == null) {
                Class.forName("com.mysql.cj.jdbc.Driver");
                String conURL = "jdbc:mysql://localhost/loja";

                Properties properties = new Properties();
                properties.setProperty("user", "root");
                properties.setProperty("allowPublicKeyRetrieval", "true");
                properties.setProperty("useSSL", "false");
                properties.setProperty("password", "");
                properties.setProperty("useTimezone", "true");
                properties.setProperty("serverTimezone", "UTC");
                con = DriverManager.getConnection(conURL, properties);
                LOGGER.info("Conexão bem sucedida");
            }
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.log(Level.SEVERE, "Erro: {}", e);
            throw new RuntimeException(e);
        }
        return con;
    }
}
